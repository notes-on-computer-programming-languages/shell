# shell

POSIX-compliant command line shell. http://get.posixcertified.ieee.org

# Package manager
* [*Is there a package management system for shell scripts?*
  ](https://askubuntu.com/questions/282713/is-there-a-package-management-system-for-shell-scripts)
  askubuntu

## Just for `bash`
### bpkg
* [www.bpkg.sh](https://www.bpkg.sh)
* [bpkg/bpkg](https://github.com/bpkg/bpkg)

### basher
* [basherpm/basher](https://github.com/basherpm/basher)

# Unofficial documentation
* [*Awesome Shell*
  ](https://project-awesome.org/alebcay/awesome-shell)

## Dialects
* [*Comparison of command shells*
  ](https://en.m.wikipedia.org/wiki/Comparison_of_command_shells)
  WikipediA
* [*Scope (computer science)*
  ](https://en.m.wikipedia.org/wiki/Scope_(computer_science)#Lexical_scoping_and_dynamic_scoping)
  Lexical scope vs. dynamic scope 
  WikipediA

## Cheatsheets
* [*Bash scripting cheatsheet*
  ](https://devhints.io/bash)

## Compatibility between shells
* [*Where are zsh and mksh incompatible with bash?*
  ](https://unix.stackexchange.com/questions/158896/where-are-zsh-and-mksh-incompatible-with-bash)
  (2019) Unix & Linux Stack Exchange

## Syntax
### Inspection
### Functions
#### Functions code introspection
##### `type`
* more portable
* `bash` version prints code, which can be use to debug code, espacially if produced through another language and strings with interpretated and escaped characters.

##### Documantation references
* [*How do I list the functions defined in my shell?*
  ](https://stackoverflow.com/questions/4471364/how-do-i-list-the-functions-defined-in-my-shell)
  (2000) Stack Overflow

### Miscellaneous
* [*What does a leading colon (:) mean in a script?*](https://aplawrence.com/Basics/leading-colon.html)
* [*set or unset options and positional parameters*](https://explainshell.com/explain?cmd=set+-euxo%20pipefail)

## Style Guides and Safety
* [*Writing Safe Shell Scripts*](https://sipb.mit.edu/doc/safe-shell/)
* [*Google Style Guides*](https://google.github.io/styleguide/)
* [*Best Practices for Writing Bash Scripts*
  ](https://kvz.io/bash-best-practices.html)
  2013-11 Kev
* [*Writing Robust Bash Shell Scripts*
  ](https://www.davidpashley.com/articles/writing-robust-shell-scripts/)
  (2019) David Pashley
* [*don’t pipe curl to bash*
  ](https://blog.dijit.sh/don-t-pipe-curl-to-bash)
  2016-11 Jan Harasym
* [*How can I trust this git repository?*
  ](https://anarc.at/blog/2020-03-17-git-gpg-verification/)
  2020-03 anarcat

## Initialisation and exit actions
* [shell initialization files](https://google.com/search?q=shell+initialization+files)
* [*Shell initialization files*](http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_03_01.html)
  (LDP/Bash-Beginners-Guide)
* [*Understanding Shell Initialization Files and User Profiles in Linux*
  ](https://www.tecmint.com/understanding-shell-initialization-files-and-user-profiles-linux/)
  2017-04 Aaron Kili
* [shell logout file](https://google.com/search?q=shell+logout+file)
* [*Linux / UNIX: Run Commands When You Log Out*
  ](https://www.cyberciti.biz/faq/linux-unix-run-commands-when-you-log-out/)
  2010-11

### /etc/profiles (/etc/profile.d/)
* login
* sh familiy
* ~/.profile read by bash in the absence of ~/.bash_profile and ~/.bash_login
* usually set the shell variables PATH, USER, MAIL, HOSTNAME and HISTSIZE

## String (manipulation)
* [*Bash String Manipulation Examples – Length, Substring, Find and Replace*
  ](https://www.thegeekstuff.com/2010/07/bash-string-manipulation/)
  2010-07 Sasikala

# Utilities you may use inside of shells...
* [coreutils](https://tracker.debian.org/pkg/coreutils)
* [moreutils](https://tracker.debian.org/pkg/moreutils)
  * [manpages](https://manpages.debian.org/unstable/moreutils/index.html)
* [busybox](https://tracker.debian.org/pkg/busybox)
* [9base](https://tracker.debian.org/pkg/9base)
* [bsdmainutils](https://tracker.debian.org/pkg/bsdmainutils)
* ...

## Examples
* [*Shell Scripting: Generate or Print Range of Numbers ( Sequence of Numbers for Loop )*
  ](https://www.cyberciti.biz/tips/how-to-generating-print-range-sequence-of-numbers.html)

# Libraries
* [*BASH3 Boilerplate*](https://bash3boilerplate.sh/)

# Examples
## Ansi colors
echo -e '\u001b[31mHello\u001b[0mWorld'

# Bugs
* [gitlab shell trap not working](https://google.com/search?q=gitlab+shell+trap+not+working)
